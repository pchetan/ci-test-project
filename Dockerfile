FROM node:14-alpine

ENV APP_HOME /app

RUN mkdir $APP_HOME
WORKDIR $APP_HOME

COPY package* ./
RUN npm install

# Add `/app/node_modules/.bin` to $PATH
ENV PATH $APP_HOME/node_modules/.bin:$PATH

COPY . ./

CMD ["npm", "start"]